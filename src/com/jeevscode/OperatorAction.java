package com.jeevscode;

public interface OperatorAction {

    public void walk();
    public void run();
    public void shoot();
    public void useGadget();
}
