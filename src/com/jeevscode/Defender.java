package com.jeevscode;

public class Defender extends Operator {

    public Defender(int speed, int armor, int health, String name) {
        super(speed, armor, health, name);
    }

    @Override
    public boolean isAttacker() {

        return false;
    }
}
