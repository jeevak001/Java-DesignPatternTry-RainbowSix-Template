package com.jeevscode;

public class Attacker extends Operator {

    public Attacker(int speed, int armor, int health, String name) {
        super(speed, armor, health, name);
    }


    @Override
    public boolean isAttacker() {

        return true;
    }
}
