package com.jeevscode;

public class Operator implements OperatorAction{


    private int speed;
    private int armor;
    private String name;
    private int health;


    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getArmor() {
        return armor;
    }

    public void setArmor(int armor) {
        this.armor = armor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }


    public Operator(int speed,int armor, int health, String name){

        this.armor = armor;
        this.health = health;
        this.name = name;
        this.speed = speed;

        createOperator();
    }

    private void createOperator() {

        walk();
        run();
        shoot();

        if (isAttacker()){

            useDrone();
            rappel();

        }else{

            useCamera();
            reinforce();

        }


    }


    public boolean isAttacker(){

        return true;
    }

    public void reinforce(){

        System.out.println("I am reinforcing wall...");
    }

    public void rappel(){

        System.out.println("I am rappelling...");
    }

    public void useCamera(){

        System.out.println("I am using Camera...");
    }

    public void useDrone(){

        System.out.println("I am using Drone...");
    }


    @Override
    public void walk() {

        System.out.println("I am walking...");
    }

    @Override
    public void run() {

        System.out.println("I am running...");
    }

    @Override
    public void shoot() {

        System.out.println("I am shooting...");
    }

    @Override
    public void useGadget() {

        System.out.println("I am using my Gadget...");
    }
}
